//= require jquery.min
//= require jquery_ujs
//= require bootstrap.bundle.min
//= require clipboard.min


function initCopyText() {
    var btn = document.getElementById('copy-btn');
    var clipboard = new ClipboardJS(btn);

    clipboard.on('success', function (e) {
        alert('Copied!');
    });
    clipboard.on('error', function (e) {
        alert('error');
    });
}
