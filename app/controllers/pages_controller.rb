
class PagesController < ApplicationController
  def start

    require 'scrambler'

    phrase = params[:phrase]
    if phrase
      contents = phrase == '' ? "Come on, you got nothing? 🤨" : phrase
      @results = Scrambler.process(contents)
    end

    respond_to do |format|
      format.html
      format.js
    end
  end


  def image
    kit = IMGKit.new("<html><head></head><body>hello</body></html>", width: 480, height: 800, quality: 100)
    respond_to do |format|
      format.png do
        send_data(kit.to_jpg, type: "image/png", disposition: 'inline')
      end
    end
  end
end
