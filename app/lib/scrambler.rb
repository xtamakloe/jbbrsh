module Scrambler

  DELIMITER = '*'
  SYMBOLS   = %w(. , ; - : ! ?)


  # Scrambles phrases
  #
  # Test cases: hello, *hello*
  def self.process(phrase)
    words = phrase.split(' ')

    results = []

    words.each do |word|
      results << (is_scrambleable?(word) ? scramble_word(word) : remove_delimiters(word))
    end

    results.join(' ')
  end


  # Scrambles words
  # Numbers => ignore
  # 2 letter words => reverse
  # 3 letter words => TODO: reverse last two letters
  # Proper nouns proper nouns e.g. names => use delimiters
  # Special characters 1. apostrophe => scramble part before quotes e.g. 's, 'll, 'd
  #                    2. single/double quotes => scramble contents
  #                    3. symbols at end of word e.g. however, => scramble word
  #                    4. symbols on their own e.g. - => ignore
  #
  # Test cases: hello, hello's, 'hello', "hello"
  def self.scramble_word(word)

    processable_word, type, special_chars = get_processable_word(word)
    is_two_letter_word                    = processable_word.length == 2
    is_three_letter_word                  = processable_word.length == 3
    processable_chars                     = get_processable_chars(word) # chars available to scramble
    char_count                            = processable_chars.length

    # conditions
    no_digits        = processable_chars.match(/\d/).nil?
    more_than_1_char = char_count > 1
    is_two_chars     = char_count == 2

    result =
      if no_digits && more_than_1_char

        char_array = processable_chars.scan(/\w/)

        if is_two_chars
          char_array.reverse!
        else
          char_array.shuffle!
        end

        scrambled = char_array.join

        unless is_two_letter_word
          scrambled.insert(0, processable_word.first) << processable_word.last
        end

        scrambled
      else
        processable_word
      end

    case type
    when :quote
      "#{special_chars}#{result}#{special_chars}"
    when :apos, :sym
      "#{result}#{special_chars}"
    else
      result
    end
  end


  def self.is_scrambleable?(word)
    # Ignore words surrounded by delimiter
    # => word.match(/^\*.*\*$/).nil? for asterisk (*)

    delimiter  = '\ '.strip + DELIMITER
    expression = "^#{delimiter}.*#{delimiter}$"

    word.match(/#{expression}/).nil?
  end


  # Get word that can be processed by removing symbols etc.
  # Returns word, :quote/:sym/:apos/nil, chars
  def self.get_processable_word(word)

    # TODO: trim word
    # TODO: explore reversal as option to scramble

    if word.match(/^".*"$/).present? || word.match(/^'.*'$/).present?
      # quotes e.g. 'hello', "hello"
      # Store quotes
      char = word[0]
      # Remove quotes
      word[0]  = ''
      word[-1] = ''

      # { word: word, type: :quote, chars: char }
      [word, :quote, char]

    elsif word.match(/.*'s$/i).present? || # => ends with 's
      word.match(/.*'d$/i).present? || # => ends with 'd
      word.match(/.*'ll$/i).present? || # => ends with 'll
      word.match(/.*'ve$/i).present? # => ends with 've

      # Store ending type
      chars = word[word.index("'")..-1]

      # Remove ending
      word[word.index("'")..-1] = ''

      [word, :apos, chars]

    elsif SYMBOLS.include?(word) # only symbol present e.g. -
      [word, nil, nil]

    elsif word.ends_with?(*SYMBOLS).present? # word ends with symbol e.g. hi!

      # Store ending char
      char = word[-1]

      # Remove ending
      word[-1] = ''

      [word, :sym, char]
    else

      [word, nil, nil]
    end
  end


  # Return chars available to scramble. Ignore symbols
  def self.get_processable_chars(word)
    return word if word.length == 2

    SYMBOLS.include?(word) ? word : word[1...-1]
  end


  def self.remove_delimiters(word)
    word.gsub(DELIMITER, '')
  end
end