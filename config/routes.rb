Rails.application.routes.draw do

  match 'start', to: 'pages#start', via: [:get, :post]

  post 'image', to: 'pages#image', as: :image

  root 'pages#start'
end
